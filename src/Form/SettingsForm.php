<?php

namespace Drupal\fathom_analytics\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form for Fathom Analytics settings.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'fathom_analytics_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'fathom_analytics.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('fathom_analytics.settings');

    $form['site_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Site ID'),
      '#description' => $this->t('Go to Fathom Analytics <a href=":url">settings page</a> to get your Site ID.', [
        ':url' => 'https://app.usefathom.com/#/settings/sites',
      ]),
      '#maxlength' => 150,
      '#size' => '20',
      '#default_value' => $config->get('site_id'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $this->config('fathom_analytics.settings')
      ->set('site_id', $form_state->getValue('site_id'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
